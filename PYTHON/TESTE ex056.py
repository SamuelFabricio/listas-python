media=0
mulher=0
homem=0
mediamulher=0
mediahomem=0
homemvelho=0
nomevelho=''
mulhervelha=0
nomevelha=''

num=int(input('\033[1;31m|Quantas pessoas serão analisadas|'))
for c in range(1,num+1):
    nome=str(input('\033[1;30m|NOME|')).upper().strip()
    idade=int(input('|IDADE|'))
    sexo=str(input('|SEXO[M/F]|'))
    if c==1 and sexo in 'Mm':
        homemvelho=idade
        nomevelho=nome
    if sexo in 'Mm' and idade>homemvelho:
        homemvelho=idade
        nomevelho=nome
    if c==1 and sexo in 'Ff':
        mulhervelha=idade
        nomevelha=nome
    if sexo in 'Ff' and idade>mulhervelha:
        mulhervelha=idade
        nomevelha=nome
    if sexo in 'Mm':
        homem=homem+1
    if sexo in 'Ff':
        mulher=mulher+1
    if sexo in 'Mm':
        mediahomem+=idade
    if sexo in 'Ff':
        mediamulher+=idade
    media += idade
    nmedia = media/num
mh=mediahomem/homem
mm=mediamulher/mulher
print('\033[1;31;47m|A média de idade é {:.1f}|'.format(nmedia))
print('|São {} Homens|'.format(homem))
print('|São {} Mulheres|'.format(mulher))
print('|A média de idade entre os homens é {:.1f}|'.format(mh))
print('|A média de idade entre as mulheres é {:.1f}|'.format(mm))
print('|O homem mais velho é o {} com {} anos|'.format(nomevelho,homemvelho))
print('|A mulher mais velha é a {} com {} anos|'.format(nomevelha,mulhervelha))