from random import randint
from time import sleep
print('VAMOS JOGAR?')
sleep(2)
n=randint(0,9999)
print('O número {} é ímpar ou par?'.format(n))
sleep(2)
print('PROCESSANDO...')
sleep(3)
k=n%2
if k==0:
    print('O número {} é par!'.format(n))
else:
    print('O número {} é ímpar!'.format(n))
sleep(2)
print('¨¨'*14)
print('MUITO FÁCIL NÃO É MESMO!!!')
