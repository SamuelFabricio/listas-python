num=int(input('|DIGITE UM NÚMERO INTEIRO|'))
print('''|ESCOLHA UMA DAS BASES PARA CONVERSÃO|
|1| |PARA CONVERTER EM BINÁRIO|
|2| |PARA CONVERTER EM OCTAL|
|3| |PARA CONVERTER EM HEXADECIMAL|''')
opçao=int(input('|DIGITE A OPÇÃO ESCOLHIDA|'))
if opçao==1:
    print('|{} em binário é {}|'.format(num,bin(num)[2:]))
elif opçao==2:
    print('|{} em octal é {}|'.format(num,oct(num)[2:]))
elif opçao==3:
    print('|{} em hexadecimal é {}|'.format(num,hex(num)[2:]))
else:
    print('|Opção inválida|')