print('\033[1;30;43m{}'.format(' '*100))
n=float(input('\033[m\033[1;30m{}Digite um número:'.format(' '*48)))
print('\033[1;30;43m{}'.format(' '*100))
print('\033[43m{}O dobro de {} é {},'
      '\n{}O triplo de {} é {},'
      '\n{}A raiz quadrada de {} é {:.1f}.'.format((' '*46),n,n*2,(' '*45),n,n*3,(' '*44),n,n**(1/2)))

