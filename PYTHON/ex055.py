maior=0
menor=0
for pesquisa in range(1,6):
    peso=float(input('\033[1;30m|DIGITE O PESO DA {}ª PESSOA|'.format(pesquisa)))
    if pesquisa==1:
        maior=peso
        menor=peso
    else:
        if peso>maior:
            maior=peso
        elif peso<menor:
            menor=peso
print('\033[1;30;47m|O MAIOR PESO É {:.1f}(kg)|\033[m'.format(maior))
print('\033[1;30;47m|O MENOR PESO É {:.1f}(kg)|\033[m'.format(menor))