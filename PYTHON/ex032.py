from random import randint
from time import sleep
print('<>'*18)
r=randint(1,2099)
print('Como saber se o ano {} é bissexto?'.format(r))
sleep(2)
print('PROCESSANDO...')
sleep(3)
if r%4==0 and r%100!=0 or r%400==0:
    print('O ano {} realmente é bissexto!'.format(r))
else:
    print('O ano {} não é bissexto!'.format(r))
print('-----------FIM-----------')