import pygame
pygame.mixer.init()
pygame.init()
pygame.mixer.music.load('ex029.mp3')
pygame.mixer.music.play()
pygame.event.wait()

from random import randint
from time import sleep
from math import ceil
print('\033[1;30;46m->RADAR<-'*3)
v=randint(60,120)
print('VELOCÍMETRO {}KM/H'.format(v))
print('PROCESSANDO...')
sleep(4)
print('AGUARDE!')
sleep(2)
if v<81:
    print('DENTRO DO LIMITE DE VELOCIDADE!')
else:
    print('VELOCIDADE ACIMA DO PERMITIDO!'
          '\nMULTA DE R${:.2f}!'
          '\nReceberá {} pontos na habilitação!'.format((v-80)*7,(ceil(v-80))//3))

