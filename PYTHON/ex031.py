from random import randint
from time import sleep
print('<>'*18)
print('QUANTOS QUILOMETROS TERÁ SUA VIAGEM?')
sleep(2)
x=randint(30,1000)
print('¨¨'*18)
print('|SUA VIAGEM É DE {}KM!|'.format(x))
sleep(3)
if x<200:
    print('PARA ESTA VIAGEM VOCÊ GASTARÁ R${:.2f}'.format((x/10)*4))
else:
    print('PARA ESTA VIAGEM VOCÊ GASTARÁ R${:.2f}'.format(((x/13)*4)+18.5))
