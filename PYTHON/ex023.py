n=int(input('Digite um número entre 0 e 9999:'))
print('O número {} é dividido da seguinte forma:'.format(n))
u=n//1%10
d=n//10%10
c=n//100%10
m=n//1000%10
print('{} Milhar(es)'.format(m))
print('{} Centena(s)'.format(c))
print('{} Dezena(s)'.format(d))
print('{} Unidade(s)'.format(u))
