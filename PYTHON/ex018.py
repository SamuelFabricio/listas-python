from math import radians,sin,cos,tan
a=int(input('Digite um ângulo qualquer:'))
r=radians(a)
s=sin(r)
c=cos(r)
t=tan(r)
print('O ângulo de {}º têm:'
      '\nComo seno {:.1f};'
      '\nComo cosseno {:.1f};'
      '\nComo tangente {:.1f}.'.format(a,s,c,t))

