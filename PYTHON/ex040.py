from random import randint
n1=randint(1,10)
n2=randint(1,10)
n3=randint(1,10)
n4=randint(1,10)
media=float(((n1+n2+n3+n4)/4))
if media<5:
    print('|Sua média anual é {:.2f}|'
          '\n|Portanto você está REPROVADO|'.format(media))
elif media<7 and media>=5 :
    print('|Sua média anual é {:.2f}|'
          '\n|Portanto você está de RECUPERAÇÃO|'.format(media))
elif media>=7 :
    print('|Sua média anual é {:.2f}|'
          '\n|Portanto você está APROVADO|'.format(media))
