from random import randint
p1=randint(100,9999)
p2=randint(1,9)
total=p1+p2
print('|FORMA DE PAGAMENTO||TOTAL DE R${}|'.format(total))
print('''|1| |PARA PAGAMENTO À VISTA|
|2| |PARA PAGAMENTO NO CRÉDITO|
|3| |PARA PARCELAMENTO NO CRÉDITO|''')
opçao=int(input('|DIGITE A OPÇÃO ESCOLHIDA|'))
if opçao==1:
    print('|À VISTA| |PAGAMENTO DE R${}|'.format(total*0.9))
elif opçao==2:
    print('|CRÉDITO| |PAGAMENTO DE R${}|'.format(total))
elif opçao==3:
    print('|PARCELADO| |ESCOLHA EM QUANTAS PARCELAS|')
    parcela=int(input('|DIGITE O NÚMERO DE PARCELAS|'))
    juros=1.12
    print('|PARCELADO EM {} VEZES DE R${:.2f}| |TOTAL DE R${:.2f}|'.format((parcela),((total/parcela)*juros),(total*juros)))
else:
    print('|OPÇÃO INVÁLIDA|')