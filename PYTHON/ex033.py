from random import randint
from time import sleep
r1=randint(1,9999)
r2=randint(1,9999)
r3=randint(1,9999)
print('::'*17)
print('Vamos analisar 3 números distintos!')
sleep(2)
print('Os números escolhidos são {}, {}, {}!'.format(r1,r2,r3))
sleep(1)
print('PROCESSANDO...')
sleep(3)
#MENOR
if r1<r2 and r1<r3:
    menor=r1
if r2<r1 and r2<r3:
    menor=r2
if r3<r1 and r3<r2:
    menor=r3
#MAIOR
if r1>r2 and r1>r3:
    maior=r1
if r2>r1 and r2>r3:
    maior=r2
if r3>r1 and r3>r2:
    maior=r3
print('O número menor é {}!'.format(menor))
sleep(2)
print('O maior número é {}!'.format(maior))
sleep(2)
print('PROCESSAMENTO COMPLETO!')