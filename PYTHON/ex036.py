salario=float(input('|DIGITE O VALOR DO SALÁRIO|'))
financiamento=float(input('|DIGITE O VALOR QUE DESEJA FINANCIAR|'))
ano=int(input('|DIGITE EM QUANTOS ANOS DESEJA FINANCIAR|'))
mes=ano*12
ips=salario*0.3
parcela=financiamento/mes
if ips>parcela:
    print('|FINANCIAMENTO APROVADO|'
          '\n|O VALOR DA PARCELA SERÁ DE R${:.2f}|'.format(parcela))
elif ips<parcela:
    print('|FINANCIAMENTO NEGADO|'
          '\n|O VALOR DA PARCELA DE R${:.2f} ULTRAPASSOU O LIMITE DE 30% DO SEU SALÁRIO DE {:.2f}|'.format((parcela),(ips)))
elif ips==parcela:
    print('|FINANCIAMENTO APROVADO|'
          '\n|O VALOR DA PARCELA SERÁ DE R${:.2f}|'.format(parcela))