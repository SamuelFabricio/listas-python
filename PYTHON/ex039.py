from datetime import date
ano=int(input('|DIGITE O ANO DE NASCIMENTO|'))
anoatual=date.today().year
alistamento=anoatual-ano
if alistamento==18 :
    print('Você deve se alistar neste ano.')
elif alistamento<18:
    print('Você deverá se alistar no ano de {},'
          ' ainda falta {} anos para você se alistar.'.format((anoatual)+(18-alistamento),(18-alistamento)))
elif alistamento>18:
    print('Você deveria ter se alistado em {},'
          'você está à {} anos com o alistamento atrasado.'.format((anoatual)-(alistamento-18),(alistamento-18)))