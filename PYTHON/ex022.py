#n=str(input('Digite o seu nome:')).strip()
#u=n.upper()
#l=n.lower()
#q=n.split()
#w=(''.join(q))
#j=len(w)
#y=n.split()
#x=y[0].upper()
#h=len(x)
#print('Seu nome em letras maiúsculas é {};'
#      '\nSeu nome em letras minúsculas é {};'
#      '\nSeu nome têm {} letras;'
#      '\nSeu primeiro nome é {} e têm {} letras.'.format(u,l,j,x,h))
n=str(input('Digite seu nome:')).strip()
#Usei strip para tirar espaços vazios no começo e no fim;
print('Seu nome com letras maiúsculas é {};'.format(n.upper()))
print('Seu nome com letras minúsculas é {};'.format(n.lower()))
#Usei upper e lower, uso comum;
print('Seu nome têm {} letras;'.format(len(n)-n.count(' ')))
#Usei count para encontrar o primeiro espaço e '-' para tirar do restante de n;
#x=n.split()
#z=x[0]
#print('Seu nome primeiro nome é {} e têm {} letras.'.format(z.upper(),n.find(' ')))
#Usei split para separar os nomes,'z' para identificar o primeiro nome, e find para contar até o primeiro espaço;
s=n.split()
print('Seu primeiro nome é {} e têm {} letras.'.format(s[0].upper(),len(s[0])))
