from time import sleep
print('\033[1;30m|DIGITE 2 NÚMEROS E DEPOIS ESCOLHA SE QUER:|'
      '\n|SOMAR/MULTIPLICAR/SABER O MAIOR|')
n1=int(input('|1ºNúmero|'))
n2=int(input('|2ºNúmero|'))
menu=1
while menu!=5:
    print( '|1| SOMAR'
           '\n|2| MULTIPLICAR'
           '\n|3| MAIOR'
           '\n|4| REINICIAR'
           '\n|5| FECHAR' )
    menu = int( input( '|MENU|:' ) )
    if menu==1:
        resultado=n1+n2
        print('|{}+{}|={}'.format(n1,n2,resultado))
        sleep(1)
    elif menu==2:
        resultado=n1*n2
        print('|{}*{}|={}'.format(n1,n2,resultado))
        sleep(1)
    elif menu==3:
        if n1>n2:
            print('|{} é o maior|'.format(n1))
        else:
            print('|{} é o maior|'.format(n2))
        sleep(1)
    elif menu==4:
        print('Digite novos números:')
        n1 = int( input( '|1ºNúmero|' ) )
        n2 = int( input( '|2ºNúmero|' ) )
    elif menu==5:
        print('Finalizando...aguarde!')
        sleep(2)
    else:
        print('Opção inválida!')
print('|PROGRAMA FINALIZADO|')


