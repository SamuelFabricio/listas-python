
print('\033[1;30;47m{}|CALCULADORA DE MASSA CORPORAL|™'.format(' '*40))
peso=float(input('\033[m\033[1;30m{}|DIGITE SEU PESO(KG)|'.format(' '*45)))
altura=float(input('{}|DIGITE SUA ALTURA(m)|'.format(' '*45)))
imc=(peso/(altura**2))
print('\033[1;30;47m{}|Seu índice de massa corporal é {:.2f}|'.format((' '*37),(imc)))
if imc<18.5 or imc==18.5:
    print('{}'.format('⇋|VOCÊ ESTÁ ABAIXO DO PESO|'*10))
elif 25>imc>18.5 or imc==25:
    print('{}'.format('⇋|VOCÊ ESTÁ COM PESO NORMAL|'*10))
elif 30>imc>25 or imc==30:
    print('{}'.format('⇋|VOCÊ ESTÁ COM OBESIDADE GRAU 1|'*10))
elif 40>imc>30 or imc==40:
    print('{}'.format('⇋|CUIDADO|⇋|VOCÊ ESTÁ COM OBESIDADE GRAU 2|'*10))
elif imc>40:
    print('{}'.format('⇋|ATENÇÃO|⇋|VOCÊ ESTÁ COM OBESIDADE GRAU 3|'*10))


