artilheiro=0
nomeartilheiro=''
timeartilheiro=''

timemediagol=''
mediagol=0
nomemediagol=''

timetitulo=''
nometitulo=''
titulo=0

timeko=''
nomeko=''
ko=0

timekt=''
nometk=''
kt=0

timethebest=''
nomethebest=''
thebestwins=0
for espaço1 in range( 0, 22 ):
    print( '\033[47m{}'.format( ' ' * 50 ) )
num=int(input('\033[m\033[1;30m|Nº|Candidatos ao The Best of the World|'))
for thebestoftheworld in range(1,num+2):
    print('\033[1;36;47m{}\033[1;30;47m⍦ {}{} ⍦\033[m\033[1;36;47m{}\033[m'.format(('<>'*19),(thebestoftheworld),('ºCandidato ao The Best of the World'),('<>'*19)))
    for espaço in range(0,22):
        print('\033[47m{}'.format(' '*50))
    nome=str(input('\033[m\033[1;30m|NOME|'))
    time=str(input('|TIME|'))
    partidas=int(input('|PARTIDAS|'))
    goltime=int(input('|GOLS TIME|'))
    golseleçao=int(input('|GOLS SELEÇÃO|'))
    tlnacional=int(input('|TÍTULO DE LIGA NACIONAL|'))
    ktlnacional=float(tlnacional*1.5)
    gln=int( input( '|GOLS LIGA NACIONAL|' ) )
    kgln = float( gln * 1.5 )
    tcnacional=int(input('|TÍTULO COPA NACIONAL|'))
    ktcnacional=float(tcnacional*1.2)
    gcn = int( input( '|GOLS COPA NACIONAL|' ) )
    kgcn = float( gcn * 1.3 )
    continente = int( input( '[1]|EUROPA|'
                             '\n[2]|AMÉRICA DO SUL|' ) )
    if continente == 2:
        tcontinental=int(input('|TÍTULO LIBERTADORES|'))
        ktcontinental=float(tcontinental*1.9)
        gnl = int( input( '|GOLS NA LIBERTADORES|' ) )
        kgcontinental= float( gnl * 1.9 )
    else:
        tcontinental=int(input('|TÍTULO CHAMPIONS|'))
        ktcontinental=float(tcontinental*2.4)
        gcl = int( input( '|GOLS NA CHAMPIONS|' ) )
        kgcontinental = float( gcl * 2.4 )
    tmundial=int(input('|TÍTULO MUNDIAL DE CLUBES|'))
    ktmundial=float(tmundial*2)
    gmundial = int( input( '|GOLS NO MUNDIAL DE CLUBES|' ) )
    kgmundial = float( gmundial * 2 )
    tcopadomundo=int(input('|TÍTULO COPA DO MUNDO|'))
    ktcopadomundo=float(tcopadomundo*2.6)
    gcup=int(input('|GOLS NA COPA DO MUNDO|'))
    kgcup=float(gcup*2.6)
    kgolseleçao=float(golseleçao*1.6)
    kok=float(kgln+kgcn+kgcontinental+kgcup+kgmundial+kgolseleçao)/6
    ktk=float(ktlnacional+ktcnacional+ktcontinental+ktmundial+ktcopadomundo)/5
    goltemporada=goltime+golseleçao
    media=float(goltemporada/partidas)
    titulos=tlnacional+tcnacional+tcontinental+tmundial+tcopadomundo
    thebest=(kok)*(1+ktk)
    for list in range(1,num+1):
        print('|{}-{} |{}títulos |{}partidas |{}gols |{}média de gols |{}média gol/importante |{}média/título |{}média TheBest|'.format(nome,time,titulos,partidas,goltemporada,media,kok,ktk,thebest))
    if thebestoftheworld==1:
        thebestwins=thebest
        nomethebest=nome
        timethebest=time
    if thebest>thebestwins:
        thebestwins=thebest
        nomethebest=nome
        timethebest=time
    if thebestoftheworld==1:
        kt=ktk
        nomekt=nome
        timekt=time
    if ktk>kt:
        kt=ktk
        nomekt=nome
        timekt=time
    if thebestoftheworld==1:
        ko=kok
        nomeko=nome
        timeko=time
    if kok>ko:
        ko=kok
        nomeko=nome
        timeko=time
    if thebestoftheworld==1:
        titulo=titulos
        nometitulo=nome
        timetitulo=time
    if titulos>titulo:
        titulo=titulos
        nometitulo=nome
        timetitulo=time
    if thebestoftheworld==1:
        artilheiro=goltemporada
        nomeartilheiro=nome
        timeartilheiro=time
    if goltemporada>artilheiro:
        artilheiro=goltemporada
        nomeartilheiro=nome
        timeartilheiro=time
    if thebestoftheworld==1:
        mediagol=media
        nomemediagol=nome
        timemediagol=time
    if media>mediagol:
        mediagol=media
        nomemediagol=nome
        timemediagol=time
print('\033[1;30;47m|QUEM FEZ MAIS GOLS NA TEMPORADA FOI {}-{} COM {} GOLS|'.format((nomeartilheiro),(timeartilheiro),(artilheiro)))
print('|A MELHOR MÉDIA DE GOLS FOI DE {}-{} COM MÉDIA DE {:.2f}|'.format((nomemediagol),(timemediagol),(mediagol)))
print('|QUEM CONQUISTOU MAIS TÍTULOS FOI {}-{} COM {} TÍTULOS|'.format((nometitulo),(timetitulo),(titulo)))
print('|QUEM TEM A MELHOR MÉDIA DE GOLS IMPORTANTES É {}-{} COM MÉDIA {}|'.format((nomeko),(timeko),(ko)))
print('|QUEM TEM A MELHOR MÉDIA DE TÍTULOS IMPORTANTES É {}-{} COM MÉDIA {}|'.format((nomekt),(timekt),(kt)))
print('|O MELHOR JOGADOR DO MUNDO É {}-{} COM MÉDIA TÍTULO/GOL {}'.format((nomethebest),(timethebest),(thebestwins)))




