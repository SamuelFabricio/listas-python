n=int(input('Digite um número:'))
c='S'
contador=1
recebidos=n
maior=n
menor=n
while c not in 'N':
    c=str(input('Deseja continuar?')).strip().upper()[0]
    if c=='S':
        n = int( input( 'Digite um número:' ) )
        contador+=1
        recebidos=recebidos+n
        if n>maior:
            maior=n
        elif n<menor:
            menor=n
    elif c=='N':
        print('Finalizando programa!')
    else:
        print('Erro!')
media=float(recebidos/contador)
print('Foram digitados {} números que somados é igual a {}.'
      '\nA média entre eles é {:.1f}, o maior é {} e o menor {}.'.format(contador,recebidos,media,maior,menor))
