num=int(input('\033[1;30m|DIGITE UM NÚMERO|'))
print('\033[1;37m{}\033[m'.format('-=-'*(num+5)))
dividi=0
for k in range(1,num+1):
    if num%k==0:
        print('\033[32m{}\033[m'.format(k),end=' ')
        dividi+=+1
    else:
        print('\033[1;33m{}\033[m'.format(k),end=' ')
else:
    if dividi==2:
        print('\033[1;33m|NÚMERO PRIMO|\033[m')
    else:
        print('\033[1;32m|NÃO É NÚMERO PRIMO|\033[m')
print('\033[1;37m{}\033[m'.format('-=-'*(num+5)))
