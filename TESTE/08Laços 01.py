from time import sleep
print('\033[1;30;47m|CONTAGEM REGRESSIVA|\033[m')
for cont in range(10,-1,-1):
    sleep(1)
    print('\033[1;31;47m{}{}{}\033[m'.format((' '*10),(cont),(' '*9)))
sleep(1)
print('\033[1;30;47m  |FELIZ ANO NOVO|  \033[m')