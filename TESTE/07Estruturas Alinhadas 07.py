from random import randint
p1=randint(200,9999)
p2=randint(1,3)
preço=1000
print('|TOTAL DA COMPRA R${:.2f}|'.format(preço))
forma=int(input('|DIGITE 1| |PARA PAGAMENTO À VISTA|'
                '\n|DIGITE 2| |PARA PAGAMENTO NO CRÉDITO|'
                '\n|DIGITE 3| |PARA 2 VEZES NO CRÉDITO|'
                '\n|DIGITE 4| |PARA 3 OU MAIS VEZES NO CRÉDITO|'))
if forma==1:
    print('|VALOR DE R${:.2f}|'.format(preço*0.9))
elif forma==2:
    print('|VALOR DE R${:.2f}|'.format(preço*0.95))
elif forma==3:
    print('|VALOR DUAS VEZES DE R${:.2f}|'.format(preço/2))
elif forma==4:
    vezes=int(input('|DIGITE O NÚMERO DE VEZES QUE DESEJA PARCELA|'))
    k1=float(preço*1.2)
    print('|VALOR DE R${:.2f}|PARCELAS DE R${:.2f}|'.format((k1),(k1/vezes)))