lista=('Onix',40000,'HB20',50000,'Argo',38000,
       'Ford Ka',42000,'Mobi',34000)
print('-'*42)
print(f'{"Lista dos valores de automóveis":^42}')
print('-'*42)
for item in range(0,len(lista)):
    if item%2==0:
        print(f'{lista[item]:.<30}',end='')
    else:
        print(f'R${lista[item]:>6},00')
print('-'*42)
