campeonato=('Liverpool','Manchester City','Chelsea','Leicester',
            'Manchester United','Wolverhampton','Sheffield United',
            'Tottenham','Arsenal','Burnley','Everton','Southampton',
            'Newcastle','Crystal Palace','Brighton','West Ham',
            'Watford','Bournemouth','Aston Villa','Norwich')
print('=-'*160)
print(f'|Lista de times da Premier Ligue:{campeonato}|')
print('=-'*160)
print(f'|Os cincos primeiros são:{campeonato[:5]}|')
print('=-'*52)
print(f'|Os quatro últimos são:{campeonato[-4:]}|')
print('=-'*160)
print(f'|Times em ordem alfabética:{sorted(campeonato)}|')
print('=-'*160)
posição=((campeonato.index('Arsenal')+1))
print(f'|O Arsenal está na {posição}ªposição|')
print('=-'*16)