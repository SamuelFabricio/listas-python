revisao=('U','S','L','A','T','P')
print(revisao[-2],revisao[0],revisao[-1],revisao[2],revisao[-3],revisao[1])

'''n=('zero','um','dois','três','quatro','cinco',
   'seis','sete','oito','nove','dez')
num=''
while True:
    num=int(input('Digite um número entre 0 e 10: '))
    if -1<num<11:
        break
print(f'Você digitou {n[num]}.')'''

'''campeonato=('Liverpool','Manchester City','Chelsea','Leicester',
            'Manchester United','Wolverhampton','Sheffield United',
            'Tottenham','Arsenal','Burnley','Everton','Southampton',
            'Newcastle','Crystal Palace','Brighton','West Ham',
            'Watford','Bournemouth','Aston Villa','Norwich')
print(f'Os cinco primeiros colocados são:{campeonato[:5]}')
print(f'Os quatro últimos colocados são:{campeonato[-4:]}')
print(f'Os times do campeonato em ordem alfabética:{sorted(campeonato)}')
print(f'O Everton está na {campeonato.index("Everton")+1}ªposição no campeonato.')'''

'''from random import randint
num=(randint(0,10),randint(0,10),randint(0,10),randint(0,10),randint(0,10))
print(f'Os números sorteados são:{num}.')
print(f'O maio número é {max(num)}')
print(f'O menor número é {min(num)}')'''

'''num=(int(input('Digite um número: ')),int(input('Digite um número: ')),
     int(input('Digite um número: ')),int(input('Digite um número: ')))
print(f'Os números digitados são:{num}')
print(f'O número 9 foi digitado {num.count(9)} vezes.')
if 3 in num:
    print(f'O número 3 foi digitado primeiro na {num.index(3)+1}ª posição.')
else:
    print('O número 3 não foi digitado.')
print('Os números pares são:',end=' ')
for n in num:
    if n%2==0:
        print(f'{n}',end=' ')'''

'''produto=('notebook',2500,'mouse',35,'teclado',120,'suporte',250)
print('-'*30)
print(f'{"Lista de Compra":^30}')
print('-'*30)
for p in range(0,len(produto)):
    if p%2==0:
        print(f'{(produto[p]).upper():.<20}',end='')
    else:
        print(f'R${produto[p]:>8.2f}')
print('-'*30)'''

'''tupla=('aprender','estudar','programar','dedicaçao','esforço','objetivo',
         'persistencia','foco')
for palavra in tupla:
    print(f'\nA palavra {(palavra).upper()} têm as vogais',end=' ')
    for letra in palavra:
        if letra in 'aeiou':
            print(letra,end='|')'''
