n1=float(input('Qual é a largura da parede?'))
n2=float(input('Qual é a altura da parede?'))
a=float(n1*n2)
p=float(a*1/2)
print('Para pintar uma parede de {}m de largura e {}m de altura, \n cuja a área é {}m²,'
      ' será necessário {}L de tinta.'.format(n1,n2,a,p))
