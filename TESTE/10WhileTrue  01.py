while True:
    tabuada=int(input('Digite um número para ver a tabuada: '))
    if tabuada<0:
        break
    for c in range(0,11):
        print(f'{tabuada}x{c}={c*tabuada}')
    print('Para finalizar o programa digite um valor negativo!')
print('Programa finalizado!')