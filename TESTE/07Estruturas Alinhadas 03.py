from random import randint
n1=randint(1,10)
n2=randint(1,10)
n3=randint(1,10)
n4=randint(1,10)
media=(n1+n2+n3+n4)/4
if media>7 or media==7:
    print('|Aprovado com média anual de {:.1f}|'.format(media))
elif media>5 and media<7  or media==5:
    print('|Você deverá fazer uma prova substutiva|'
          '\n|Sua média anual foi de {:.1f}|'.format(media))
elif media<5:
    print('|Reprovado com média anual de {:.1f}|'.format(media))