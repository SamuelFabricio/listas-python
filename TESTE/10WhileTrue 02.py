from random import randint
par='par'
impar='ímpar'
jogadorescolha=''
contador=0
resultadoescolha=''
while True:
    computador = randint( 0, 10 )
    jogador=int(input('Escolha um número: '))
    resultado=computador+jogador
    if resultado%2==0:
        resultadoescolha=par
    else:
        resultadoescolha=impar
    while True:
        escolha=str(input('Par ou Ímpar?')).strip().upper()[0]
        if escolha in 'P':
            jogadorescolha=par
            break
        elif escolha in 'IÍ':
            jogadorescolha=impar
            break
        else:
            print('ERRO')
    if jogadorescolha==resultadoescolha:
        print(f'Você jogou {jogador} e eu {computador}.Total {resultado} que é {resultadoescolha}!')
        print('Você Venceu!!!')
        contador+=1
    elif jogadorescolha!=resultadoescolha:
        print(f'Você jogou {jogador} e eu {computador}.Total {resultado} que é {resultadoescolha}!')
        print('Você Perdeu!!!')
        break
print(f'O Jogador venceu {contador} vez(es) consecultiva(s)!')
