print('{}'.format(('\033[1;7;31;48m|GHS FINANCIAMENTOS SA|'*20)))
salario=int(input('\033[m\033[1;31;48m{}|DIGITE O VALOR DE SEU SALÁRIO|'.format(' '*40)))
financiamento=int(input('{}|DIGITE O VALOR DO FINANCIAMENTO|'.format(' '*39)))
ano=int(input('{}|DIGITE EM QUANTOS ANOS DESEJA FINANCIAR|'.format(' '*35)))
mes=int(ano*12)
parcela=float((financiamento/mes)*1.75)
psf=float(salario*0.30)
if psf>parcela:
    print('\033[1;7;31;48m{}|SEU FINANCIAMENTO FOI APROVADO|'.format(' '*39))
    print('{}|{} PARCELAS DE R${:.2f}||TOTAL FINANCIADO DE R${:.2f}'.format((' '*30),(mes),(parcela),(financiamento)))
elif psf<parcela:
    print('\033[1;7;31;48m{}|SEU FINANCIAMENTO FOI REPROVADO|'.format(' '*39))
elif psf==parcela:
    print('\033[1;7;31;48m{}|SEU FINANCIAMENTO FOI APROVADO|'.format(' '*39))


