n1=int(input('Escreva um número:'))
n2=n1-1
n3=n1+1
n4=n1*2
n5=n1*3
n6=(n1**(1/2))
print('Seu antecessor é {}, seu sucessor é {}!'.format(n2,n3))
print('O dobro é {}, o triplo é {}, sua raiz quadrada é {:.3f}.'.format(n4,n5,n6))
