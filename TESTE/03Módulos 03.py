from math import radians,sin,cos,tan
a=int(input('Digite um ângulo qualquer:'))
r=float(radians(a))
s=float(sin(r))
c=float(cos(r))
t=float(tan(r))
print('O ângulo de {}º em radianos vale {:.2f},'
      '\nO Seno vale {:.3f}.'
      '\nO Cosseno vale {:.3f}.'
      '\nA Tangente vale {:.3f}.'.format(a,r,s,c,t))


