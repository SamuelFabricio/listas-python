n=int(input('Digite um número para saber o fatorial:'))
c=n
f=1
print('O fatorial de {}! '.format(n),end='')
while c>0:
    print(c,end='')
    print('x' if c>1 else '=',end='')
    f*=c
    c-=1
print(f,end='')
